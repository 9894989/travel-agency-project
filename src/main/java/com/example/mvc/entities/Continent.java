package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@Entity
public class Continent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Name cannot be null")
    private String name;

     @OneToMany(mappedBy="continent", cascade = CascadeType.REMOVE, orphanRemoval = true)
     private Set<Country> countries;
}
