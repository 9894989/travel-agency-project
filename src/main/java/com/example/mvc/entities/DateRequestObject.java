package com.example.mvc.entities;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;

@Data
public class DateRequestObject {
    @Min(value = 1, message = "Date should not be less than 1")
    private Date startDate;
    @Max(value = 31, message = "Date should not be greater than 31")
    private Date endDate;
}
