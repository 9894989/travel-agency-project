package com.example.mvc.entities;

import lombok.Data;

import java.util.Date;

@Data
public class TripRequestObject {
    private Date departureDate;
    private City city;
    private Continent continent;
    private Hotel hotel;
    private Airport airport;
}
