package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "tripPurchase", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Trip> trips;

    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;
    //TOTAL puchase price

    @Column(name = "total_amount", nullable= false, precision=10, scale=2)
    private BigDecimal amount;







}
