package com.example.mvc.entities.example;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
@Data //lombok library annotation for getters and setters auto-generation
@Deprecated
public class PermissionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "Group name is mandatory")
    private String name;

}
