package com.example.mvc.entities;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    private City locationFrom;
    @OneToOne
    private City locationTo;
    @OneToOne
    private Hotel hotel;
    @OneToOne
    private Airport airport;
    @NotBlank(message = "Departure date should be present")
    private Date departureDate;
    @ColumnDefault(value = "1970-01-01")
    private Date returnDate;
    @Min(value = 1)
    private Integer numberOfDays;
    @NotBlank
    private String type;
    @Column(nullable= false, precision=10, scale=2)
    private BigDecimal adultPrice;
    @Column(nullable= false, precision=10, scale=2)
    private BigDecimal childrenPrice;

    private boolean promoted;

    @Min(value = 1)
    private Integer adultBeds;
    @Min(value = 0)
    private Integer childBeds;

     @ManyToOne(fetch = FetchType.LAZY)
     private Purchase tripPurchase;

     @OneToMany
     private List<User> tripParticipants;

     public void setPromoted(int value) {
         if (value == 0) {
             promoted = false;
         } else {
             promoted = true;
         }
     }

}
