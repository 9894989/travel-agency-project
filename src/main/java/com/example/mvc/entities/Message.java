package com.example.mvc.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class Message {
    @Size(min = 10, max = 200, message = "Message must be between 10 and 500 characters")
    private String text;
}
