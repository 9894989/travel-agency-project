package com.example.mvc.services;

import com.example.mvc.entities.Country;
import com.example.mvc.entities.ID;
import com.example.mvc.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Service
@Transactional

public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    public List<Country> findAllCountries() {
        return countryRepository.findAll();
    }

    public Country save(Country country) {
        return countryRepository.save(country);
    }

    public void delete(Country country) {
        countryRepository.delete(country);
    }

    public void deleteById(@Valid ID country) {
        countryRepository.deleteById(country.getId());
    }

    public Country findById(Long id) {
        return countryRepository.findById(id).get();
    }
}
