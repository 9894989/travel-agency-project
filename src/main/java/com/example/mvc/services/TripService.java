package com.example.mvc.services;

import com.example.mvc.entities.DateRequestObject;
import com.example.mvc.entities.Trip;
import com.example.mvc.entities.TripRequestObject;
import com.example.mvc.repositories.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TripService {

    @Autowired
    private TripRepository tripRepository;


    public List<Trip> findAllTrips() {
        return tripRepository.findAll();
    }

    public Trip save(Trip trip) {
        return tripRepository.save(trip);
    }

    public void delete(Trip trip) {
       tripRepository.delete(trip);
    }

    public void deleteById(Long trip) {
        tripRepository.deleteById(trip);
    }

    public List<Trip> findTripsByDate(LocalDate date) {
        date.plusDays(7);
        Date d = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return tripRepository.findTripsByDate(d);
    }

    public List<Trip> findTripsByDateAndContinent(TripRequestObject request) {
        return tripRepository.findTripsByDateAndContinent(request.getDepartureDate(), request.getContinent());
    }

    public List<Trip> findTripsByCity(TripRequestObject request) {
        return tripRepository.findTripsByCity(request.getCity());
    }

    public List<Trip> findTripsByHotel(TripRequestObject request) {
        return tripRepository.findTripsByHotel(request.getHotel());
    }

    public List<Trip> findTripsByAirport(TripRequestObject request) {
        return tripRepository.findTripsByAirport(request.getHotel());
    }

    public List<Trip> findTripsByDepartureDateInterval(DateRequestObject request) {
        return tripRepository.findTripsByDepartureDateInterval(request.getStartDate(), request.getEndDate());
    }

    public List<Trip> findTripsByArrivalDateInterval(DateRequestObject request) {
        return tripRepository.findTripsByArrivalDateInterval(request.getStartDate(), request.getEndDate());
    }

    public List<Trip> findTripsByNumberOfDays(Integer number) {
        return tripRepository.findTripsByLength(number);
    }


}
