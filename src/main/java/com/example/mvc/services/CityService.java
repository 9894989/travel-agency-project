package com.example.mvc.services;

import com.example.mvc.entities.City;
import com.example.mvc.entities.ID;
import com.example.mvc.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Service
@Transactional

public class CityService {
    @Autowired
    private CityRepository cityRepository;

    public List<City> findAllCities() {
        return cityRepository.findAll();
    }

    public City findById(Long id) {
        return cityRepository.findById(id).get();
    }


    public City save(City city) {
        return cityRepository.save(city);
    }

    public void delete(City city) {
        cityRepository.delete(city);
    }

    public void deleteById(@Valid ID city) {
        cityRepository.deleteById(city.getId());
    }


}
