package com.example.mvc.services;


import com.example.mvc.entities.Continent;
import com.example.mvc.entities.ID;
import com.example.mvc.repositories.ContinentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Service
@Transactional

public class ContinentService {
    @Autowired
    private ContinentRepository continentRepository;

    public List<Continent> findAllContinents() {
        return continentRepository.findAll();
    }

    public Continent findById(Long id) {
        return continentRepository.findById(id).get();
    }

    public Continent save(Continent continent) {
        return continentRepository.save(continent);
    }

    public void delete(Continent continent) {
        continentRepository.delete(continent);
    }

    public void deleteById(@Valid ID continent) {
        continentRepository.deleteById(continent.getId());
    }
}
