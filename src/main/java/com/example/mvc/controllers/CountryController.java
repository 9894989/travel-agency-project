package com.example.mvc.controllers;


import com.example.mvc.entities.Country;
import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.services.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/country")

public class CountryController {

    private final CountryService countryService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Country>> findAll() {
        List<Country> countries = countryService.findAllCountries();
        return ResponseEntity.ok(countries);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Country> findById(@PathVariable Long id) {
        Country ctr = countryService.findById(id);
        return ResponseEntity.ok(ctr);
    }

    @PostMapping("/save")
    public ResponseEntity<Country> save(@Valid @RequestBody Country country){
        Country country1 = countryService.save(country);
        return ResponseEntity.ok(country1);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody Country country){
        countryService.delete(country);
        return ResponseEntity.ok((new Message("Record deleted")));
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID country) {
        countryService.deleteById(country);
        return ResponseEntity.ok((new Message("Rexord deleted")));

    }

}
