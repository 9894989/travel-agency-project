package com.example.mvc.controllers;


import com.example.mvc.entities.Continent;
import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.services.ContinentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/continent")

public class ContinentController {

    private final ContinentService continentService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Continent>> findAll() {
        List<Continent> continents = continentService.findAllContinents();
        return ResponseEntity.ok(continents);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Continent> findById(@PathVariable Long id) {
        Continent cont = continentService.findById(id);
        return ResponseEntity.ok(cont);
    }


    @PostMapping("/save")
    public ResponseEntity<Continent> save(@Valid @RequestBody Continent continent){
        Continent continent1 = continentService.save(continent);
        return ResponseEntity.ok(continent1);
    }


    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody Continent continent){
        continentService.delete(continent);
        return ResponseEntity.ok((new Message("Record deleted")));
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID continent) {
        continentService.deleteById(continent);
        return ResponseEntity.ok((new Message("Record deleted")));

    }

}
