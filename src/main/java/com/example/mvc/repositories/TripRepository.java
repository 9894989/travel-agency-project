package com.example.mvc.repositories;

import com.example.mvc.entities.City;
import com.example.mvc.entities.Continent;
import com.example.mvc.entities.Hotel;
import com.example.mvc.entities.Trip;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {

    List<Trip> findAll();

    Optional<Trip> findById(Long id);

    Trip save(Trip t);

    @Override
    <S extends Trip> Iterable<S> saveAll(Iterable<S> iterable);

    void delete(Trip var1);

    void deleteById(Long id);

    @Query("FROM Trip WHERE departureDate = :departureDate ORDER by departureDate")
    List<Trip> findTripsByDate(@Param("departureDate") Date departureDate);

    @Query("FROM Trip WHERE numberOfDays = :numberOfDays ORDER by departureDate")
    List<Trip> findTripsByLength(@Param("numberOfDays") Integer numberOfDays);

    @Query("FROM Trip WHERE departureDate = :departureDate AND locationTo.country.continent = :continent ORDER by departureDate")
    List<Trip> findTripsByDateAndContinent(@Param("departureDate") Date departureDate, @Param("continent") Continent continent);

    @Query("FROM Trip WHERE locationTo = :city AND departureDate > current_date() ORDER by departureDate")
    List<Trip> findTripsByCity(@Param("city") City city);

    @Query("FROM Trip WHERE hotel = :hotel AND departureDate > current_date() ORDER by departureDate")
    List<Trip> findTripsByHotel(@Param("hotel") Hotel hotel);

    @Query("FROM Trip WHERE airport = :airport AND departureDate > current_date() ORDER by departureDate")
    List<Trip> findTripsByAirport(@Param("airport") Hotel hotel);

    @Query("FROM Trip WHERE departureDate BETWEEN :startDate AND :endDate ORDER by departureDate")
    List<Trip> findTripsByDepartureDateInterval(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("FROM Trip WHERE arrivalDate BETWEEN :startDate AND :endDate ORDER by departureDate")
    List<Trip> findTripsByArrivalDateInterval(@Param("startDate") Date startDate, @Param("endDate") Date endDate);


}
