package com.example.mvc.repositories;

import com.example.mvc.entities.Hotel;
import com.example.mvc.entities.ID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
    public interface HotelRepository extends CrudRepository<Hotel, Long> {
        List<Hotel> findAll();
        Optional<Hotel> findById(Long id);
        Hotel save (Hotel hotel1);
        void delete (Hotel var);
        void deleteById(Long id);
        void deleteById(ID hotel);
}
