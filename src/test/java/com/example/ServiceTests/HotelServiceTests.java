package com.example.ServiceTests;


import com.example.mvc.entities.Hotel;
import com.example.mvc.repositories.HotelRepository;
import com.example.mvc.services.HotelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class HotelServiceTests {

    @Mock
    HotelRepository hotelRepository;

    @InjectMocks
    HotelService hotelService;

    @Test
    public void testHotelSave() {

        Hotel hotelRequest = new Hotel();
        hotelRequest.setName("test hotel");

        Hotel hotelResponse = new Hotel();
        hotelResponse.setId(2L);
        hotelResponse.setName("test Hotel");

        when(hotelRepository.save(hotelRequest)).thenReturn(hotelResponse);

        Hotel created = hotelService.save(hotelRequest);

        assertTrue(created.getName().equals(hotelRequest.getName()));
        assertNotNull(created.getId());

    }

    @Test
    public void testFindAll() {
        List<Hotel> cities = List.of(new Hotel(), new Hotel(), new Hotel());

        when(hotelRepository.findAll()).thenReturn(cities);

        List<Hotel> created = hotelService.findAllHotels();

        assertNotNull(created);
        assertTrue(!created.isEmpty());
        assertTrue(created.size() == 3);

    }
}

