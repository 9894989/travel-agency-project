package com.example.ServiceTests;

import com.example.mvc.entities.Continent;
import com.example.mvc.repositories.ContinentRepository;
import com.example.mvc.services.ContinentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ContinentServiceTests {

    @Mock
    ContinentRepository continentRepository;

    @InjectMocks
    ContinentService continentService;

    @Test
    public void testContinentSave() {

        Continent continentRequest = new Continent();
        continentRequest.setName("test continent");

        Continent continentResponse = new Continent();
        continentResponse.setId(2L);
        continentResponse.setName("test continent");

        when(continentRepository.save(continentRequest)).thenReturn(continentResponse);

        Continent created = continentService.save(continentRequest);

        assertTrue(created.getName().equals(continentRequest.getName()));
        assertNotNull(created.getId());

    }

    @Test
    public void testFindAll() {
        List<Continent> continents = List.of(new Continent(), new Continent(), new Continent());

        when(continentRepository.findAll()).thenReturn(continents);

        List<Continent> created = continentService.findAllContinents();

        assertNotNull(created);
        assertTrue(!created.isEmpty());
        assertTrue(created.size() == 3);

    }
}