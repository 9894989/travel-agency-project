package com.example.ControllerTests;

import com.example.mvc.controllers.PurchaseController;
import com.example.mvc.entities.Purchase;
import com.example.mvc.services.PurchaseService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = PurchaseController.class)
public class PurchaseControllerTests {

    @MockBean
    PurchaseService purchaseService;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void it_should_return_created_purchase() throws Exception {
        Purchase request = new Purchase();
        request.setName("test purchase");

        Purchase purchase = new Purchase();
        Purchase.setName(request.getName());

        when(purchaseService.save(request)).thenReturn(purchase);
        mockMvc.perform(post("/purchases")
                .content(mapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(request.getName()));


    }
}

