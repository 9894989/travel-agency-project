package com.example.ControllerTests;

import com.example.mvc.controllers.HotelController;
import com.example.mvc.entities.Hotel;
import com.example.mvc.services.HotelService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = HotelController.class)
    public class HotelControllerTests {

        @MockBean
        HotelService hotelService;

        ObjectMapper mapper = new ObjectMapper();

        @Autowired
        private MockMvc mockMvc;

        @Test
        public void it_should_return_created_hotel() throws Exception {
            Hotel request = new Hotel();
            request.setName("test hotel");

            Hotel hotel = new Hotel();
            hotel.setName(request.getName());

            when(hotelService.save(request)).thenReturn(hotel);
            mockMvc.perform(post("/hotels")
                    .content(mapper.writeValueAsString(request))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.name").value(request.getName()));

        }
}
